package com.example.demo.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Infirmiere;
import com.example.demo.services.InfirmierService;



@RestController
@CrossOrigin
@RequestMapping("infirmieres")
public class InfirmierController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InfirmierController.class);
	
	@Autowired
	private InfirmierService infirmierService;
	
	@GetMapping("/")
	public List<Infirmiere> getAllInfirmiers(){
		LOGGER.info("Récupération de tous les infirmiers.");
		return infirmierService.findAll();
	}
	
	@GetMapping("/{id}")
	public Infirmiere getInfirmier(@PathVariable Integer id){
		LOGGER.info("Récupération d'un infirmier");
		return infirmierService.findOne(id);
	}
	
	@PostMapping("/create")
	public Infirmiere createInfirmier(@RequestBody Infirmiere infirmier) {
		LOGGER.info("Création d'un infirmier");
		return infirmierService.create(infirmier);
	}
	
	@PutMapping("/update")
	public Infirmiere updateInfirmier(@RequestBody Infirmiere infirmier) {
		LOGGER.info("Mise à jour d'un infirmier");
		return infirmierService.update(infirmier);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteInfirmier(@PathVariable Integer id) {
		LOGGER.info("Suppression d'un infirmier");
		infirmierService.delete(id);
	}
	
	

}
