package com.example.demo.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Adresse;
import com.example.demo.services.AdresseServices;

@RestController
@CrossOrigin
@RequestMapping("adresses")
public class AdresseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdresseController.class);

	
	@Autowired
	private AdresseServices adresseServices;
	
	
	@GetMapping("/")
	public List<Adresse> getAllAdresses(){
		return adresseServices.findAll();
	}
	
	@GetMapping("/{id}")
	public Adresse getAdresse(@PathVariable Integer id){
		return adresseServices.findOne(id);
	}
	
	@PostMapping("/create")
	public Adresse createAdresse(@RequestBody Adresse adresse){
		return adresseServices.create(adresse);
	}
	
	@PutMapping("/update")
	public Adresse updateAdresse(@RequestBody Adresse adresse){
		return adresseServices.update(adresse);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteAdresse(@PathVariable Integer id) {
		adresseServices.delete(id);
	}
	
	
	

}
