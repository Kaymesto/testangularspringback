package com.example.demo.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Deplacement;
import com.example.demo.services.DeplacementService;

@RestController
@CrossOrigin
@RequestMapping("deplacements")
public class DeplacementController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeplacementController.class);

	
	@Autowired
	private DeplacementService deplacementService;
	
	@GetMapping("/")
	public List<Deplacement> getDeplacements(){
		return deplacementService.findAll();
	}
	
	@GetMapping("/{id}")
	public Deplacement getDeplacement(@PathVariable Integer id) {
		return deplacementService.findOne(id);
	}
	
	@PostMapping("/create")
	public Deplacement createDeplacement(@RequestBody Deplacement deplacement) {
		return deplacementService.create(deplacement);
	}
	
	@PutMapping("/update")
	public Deplacement updateDeplacement(@RequestBody Deplacement deplacement) {
		return deplacementService.update(deplacement);
	}
	
	@DeleteMapping("delete/{id}")
	public void deleteDeplacement(@PathVariable Integer id) {
		deplacementService.delete(id);
	}

}
