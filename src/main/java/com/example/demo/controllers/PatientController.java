package com.example.demo.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Patient;
import com.example.demo.services.PatientService;

@RestController
@CrossOrigin
@RequestMapping("patients")
public class PatientController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

	
	@Autowired
	private PatientService patientService;
	
	@GetMapping("/")
	public List<Patient> getAllPatients(){
		return patientService.findAll();
	}
	
	@GetMapping("/{id}")
	public Patient getPatient(@PathVariable Integer id) {
		return patientService.findOne(id);
	}
	
	@PostMapping("/create")
	public Patient createPatient(@RequestBody Patient patient) {
		return patientService.create(patient);
	}
	
	@PutMapping("/update")
	public Patient updatePatient(@RequestBody Patient patient) {
		return patientService.update(patient);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deletePatient(@PathVariable Integer id) {
		patientService.delete(id);
	}
	
}
