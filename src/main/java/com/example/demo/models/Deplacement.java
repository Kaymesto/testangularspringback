package com.example.demo.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="deplacement")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Deplacement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Float cout;
	
	private Date date;
	
	@ManyToOne
	@JsonIgnore
    @JoinColumn(name="patient_id",referencedColumnName= "id")
	private Patient patient;
	
	@ManyToOne
	@JsonIgnore
    @JoinColumn(name="infirmiere_id",referencedColumnName= "id")
	private Infirmiere infirmiere;
}
