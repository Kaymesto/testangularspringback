package com.example.demo.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "patient")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Column(name = "nom")
	private String nom;

	@NotNull
	@Column(name = "prenom")
	private String prenom;

	@NotNull
	@Column(name = "date_de_naissance")
	private Date date_de_naissance;

	@Column(columnDefinition = "ENUM('Homme', 'Femme', 'Autre'")
    @Enumerated(EnumType.STRING)
	private Sexe sexe;

	@NotNull
	@Column(name = "numero_securite_sociale")
	private Long numero_securite_sociale;

	@ManyToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "infirmiere_id", referencedColumnName = "id")
	private Infirmiere infirmiere;

	@ManyToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "adresse_id", referencedColumnName = "id")
	private Adresse adressepatient;

	public enum Sexe {
		Homme, Femme, Autre
	}

}
