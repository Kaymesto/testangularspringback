package com.example.demo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder.Default;

@Entity
@Table(name="infirmiere")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Infirmiere {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="prenom")
	private String prenom;
	
	@Column(name="tel_pro", unique=true)
	private String tel_pro;

	@Column(name="tel_perso", unique=true)
	private String tel_perso;
		
	@Column(name="numero_professionnel", unique=true)
	private int numero_professionnel;
	
	@ManyToOne
    @JoinColumn(name="adresse_id",referencedColumnName= "id")
	private Adresse adresseinfirmiere;

	@OneToMany(mappedBy = "infirmiere")
	private List<Deplacement> deplacements;
}



