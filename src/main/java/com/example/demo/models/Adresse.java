package com.example.demo.models;

import java.util.List;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "adresse")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "numero")
	@NotNull
	private String numero;

	@Column(name = "rue")
	@NotNull
	private String rue;

	@Column(name = "cp")
	@NotNull
	private String cp;

	@Column(name = "ville")
	@NotNull
	private String ville;

	@OneToMany(mappedBy = "adresseinfirmiere", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Infirmiere> infirmiers;

	@OneToMany(mappedBy = "adressepatient", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Patient> patients;

}
