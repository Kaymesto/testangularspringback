package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.repositories.AdresseRepository;
import com.example.demo.repositories.DeplacementRepository;
import com.example.demo.repositories.InfirmierRepository;
import com.example.demo.repositories.PatientRepository;
import com.example.demo.services.AdresseServices;
import com.example.demo.services.DeplacementService;
import com.example.demo.services.InfirmierService;
import com.example.demo.services.PatientService;
import com.example.demo.services.servicesImpl.AdresseServicesImpl;
import com.example.demo.services.servicesImpl.DeplacementServiceImpl;
import com.example.demo.services.servicesImpl.InfirmierServiceImpl;
import com.example.demo.services.servicesImpl.PatientServiceImpl;

@Configuration
public class AppConfiguration {
	
	@Bean
	public PatientService patientService(PatientRepository patientRepository, AdresseServices adresseServices) {
		return new PatientServiceImpl(patientRepository, adresseServices);
	}
	
	@Bean
	public DeplacementService deplacementService(DeplacementRepository deplacementRepository) {
		return new DeplacementServiceImpl(deplacementRepository);
	}
	
	@Bean
	public AdresseServices adresseService(AdresseRepository adresseRepository) {
		return new AdresseServicesImpl(adresseRepository);
	}
	
	@Bean
	public InfirmierService infirmerieService(InfirmierRepository infirmierRepository, AdresseServices adresseServices) {
		return new InfirmierServiceImpl(infirmierRepository, adresseServices);
	}

}
