package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Infirmiere;

public interface InfirmierService {
	
	List<Infirmiere> findAll();
	
	Infirmiere findOne(Integer id);
	
	Infirmiere create(Infirmiere infirmiere);
	
	Infirmiere update(Infirmiere infirmiere);
	
	void delete(Integer id);
	
	

}
