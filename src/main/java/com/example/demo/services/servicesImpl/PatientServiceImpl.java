package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Adresse;
import com.example.demo.models.Patient;
import com.example.demo.repositories.AdresseRepository;
import com.example.demo.repositories.PatientRepository;
import com.example.demo.services.AdresseServices;
import com.example.demo.services.InfirmierService;
import com.example.demo.services.PatientService;

public class PatientServiceImpl implements PatientService {

	private PatientRepository patientRepository;

	private AdresseServices adresseServices;

	public PatientServiceImpl(PatientRepository patientRepository, AdresseServices adresseServices) {
		this.patientRepository = patientRepository;
		this.adresseServices = adresseServices;
	}

	@Override
	public List<Patient> findAll() {
		return patientRepository.findAll();
	}

	@Override
	public Patient findOne(Integer id) {
		return patientRepository.findById(id).get();
	}

	@Override
	public Patient create(Patient patient) {
		Adresse adresse = patient.getAdressepatient();
		adresse = adresseServices.create(adresse);
		patient.setAdressepatient(adresse);
		return patientRepository.save(patient);
	}

	@Override
	public Patient update(Patient patient) {
		Adresse adresse = patient.getAdressepatient();
		adresse = adresseServices.create(adresse);
		patient.setAdressepatient(adresse);
		return patientRepository.save(patient);
	}

	@Override
	public void delete(Integer id) {
		patientRepository.deleteById(id);
	}

}
