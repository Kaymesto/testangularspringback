package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Adresse;
import com.example.demo.repositories.AdresseRepository;
import com.example.demo.services.AdresseServices;

public class AdresseServicesImpl implements AdresseServices {

	private AdresseRepository adresseRepository;

	public AdresseServicesImpl(AdresseRepository adresseRepository) {
		this.adresseRepository = adresseRepository;
	}

	@Override
	public List<Adresse> findAll() {
		return adresseRepository.findAll();
	}

	@Override
	public Adresse findOne(Integer id) {
		return adresseRepository.findById(id).get();
	}

	@Override
	public Adresse create(Adresse adresse) {
		List<Adresse> allAdresses = adresseRepository.findAll();
		for (Adresse adresse2 : allAdresses) {
			if (adresse.getNumero().equals(adresse2.getNumero()) && adresse.getRue().equals(adresse2.getRue())
					&& adresse.getCp().equals(adresse2.getCp()) && adresse.getVille().equals(adresse2.getVille())) {
				return adresse2;
			}
		}
		return adresseRepository.save(adresse);
	}

	@Override
	public Adresse update(Adresse adresse) {
		return adresseRepository.save(adresse);
	}

	@Override
	public void delete(Integer id) {
		adresseRepository.deleteById(id);
	}

}
