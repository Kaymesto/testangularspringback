package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Deplacement;
import com.example.demo.repositories.DeplacementRepository;
import com.example.demo.services.DeplacementService;

public class DeplacementServiceImpl implements DeplacementService {
	
	private DeplacementRepository deplacementRepository;
	
	public DeplacementServiceImpl (DeplacementRepository deplacementRepository) {
		this.deplacementRepository = deplacementRepository;
	}

	@Override
	public List<Deplacement> findAll() {
		return deplacementRepository.findAll();
	}

	@Override
	public Deplacement findOne(Integer id) {
		return deplacementRepository.findById(id).get();
	}

	@Override
	public Deplacement create(Deplacement deplacement) {
		return deplacementRepository.save(deplacement);
	}

	@Override
	public Deplacement update(Deplacement deplacement) {
		return deplacementRepository.save(deplacement);
	}

	@Override
	public void delete(Integer id) {
		deplacementRepository.deleteById(id);
	}
	
	

}
