package com.example.demo.services.servicesImpl;

import java.util.List;

import com.example.demo.models.Adresse;
import com.example.demo.models.Infirmiere;
import com.example.demo.repositories.InfirmierRepository;
import com.example.demo.services.AdresseServices;
import com.example.demo.services.InfirmierService;

public class InfirmierServiceImpl implements InfirmierService {
	
	private InfirmierRepository infirmierRepository;
	
	private AdresseServices adresseServices;

	
	public InfirmierServiceImpl(InfirmierRepository infirmierRepository, AdresseServices adresseServices) {
		this.infirmierRepository = infirmierRepository;
		this.adresseServices = adresseServices;
	}

	@Override
	public List<Infirmiere> findAll() {
		return infirmierRepository.findAll();
	}

	@Override
	public Infirmiere findOne(Integer id) {
		return infirmierRepository.findById(id).get();
	}

	@Override
	public Infirmiere create(Infirmiere infirmiere) {
		Adresse adresse = infirmiere.getAdresseinfirmiere();
		adresse = adresseServices.create(adresse);
		infirmiere.setAdresseinfirmiere(adresse);
		return infirmierRepository.save(infirmiere);
	}

	@Override
	public Infirmiere update(Infirmiere infirmiere) {
		Adresse adresse = infirmiere.getAdresseinfirmiere();
		adresse = adresseServices.create(adresse);
		infirmiere.setAdresseinfirmiere(adresse);
		return infirmierRepository.save(infirmiere);
	}

	@Override
	public void delete(Integer id) {
		infirmierRepository.deleteById(id);
	}

}
