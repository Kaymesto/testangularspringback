package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Patient;

public interface PatientService {
	
	List<Patient> findAll();

	Patient findOne(Integer id);
	
	Patient create(Patient patient);
	
	Patient update(Patient patient);
	
	void delete(Integer id);
	
}
