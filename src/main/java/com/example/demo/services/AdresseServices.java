package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Adresse;

public interface AdresseServices {
	
	List<Adresse> findAll();
	
	Adresse findOne(Integer id);
	
	Adresse create(Adresse adresse);
	
	Adresse update(Adresse adresse);
	
	void delete(Integer id);

}
