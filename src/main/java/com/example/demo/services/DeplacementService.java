package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Deplacement;

public interface DeplacementService {
	
	List<Deplacement> findAll();

	Deplacement findOne(Integer id);
	
	Deplacement create(Deplacement deplacement);
	
	Deplacement update(Deplacement deplacement);
	
	void delete(Integer id);
	
	
}
